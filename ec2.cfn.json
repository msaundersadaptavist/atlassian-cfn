{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "This Template creates a BJ's standard server instances. Delete the DeletionPolicy line if you want the instances to be deleted when the CF template is deleted. - Updated 12/05/2017",
  "Parameters": {
    "KeyName": {
      "Description": "Name of existing EC2 KeyPair to enable SSH access to the instance",
      "Type": "AWS::EC2::KeyPair::KeyName",
      "MinLength": "2",
      "ConstraintDescription": "REQUIRED! Must be the name of an existing EC2 KeyPair.",
      "Default": "lnx-qa"
    },
    "TerminationProtection": {
      "Description": "Select 'True' to enable termination proection, preventing accidential deleting of instances.",
      "Type": "String",
      "AllowedValues": [
        "True",
        "False"
      ],
      "MinLength": "4",
      "MaxLength": "5",
      "ConstraintDescription": "You must select to enable (True) or not (False) termination proection of instances.",
      "Default": "True"
    },
    "SubNet": {
      "Description": "REQUIRED! - You will need to know the subnet ID before continuing, then select the appropriate subnet",
      "Type": "List<AWS::EC2::Subnet::Id>",
      "MinLength": "4",
      "ConstraintDescription": "Go back and select a subnet."
    },
    "SecurityGroupIds": {
      "Description": "REQUIRED! - Select the appropriate Security Group",
      "Type": "List<AWS::EC2::SecurityGroup::Id>",
      "ConstraintDescription": "Go back and select a EC2 security group."
    },
    "HostName": {
      "Description": "REQUIRED! - Enter the Hostname using BJ's standard naming convention (ue00 for Linux, we00 for Windows, for example; ue00xxxXXp01 where xxx=system/app, XX = type, and “p” for production “d” for dev; “db” for database, and the sequential number at the end.)",
      "Type": "String",
      "MinLength": "12",
      "MaxLength": "12",
      "ConstraintDescription": "Go back an edit the host name, it must contain only alphanumeric characters and 12 characters and conform to BJ's standards."
    },
    "CreatedBy": {
      "Description": "REQUIRED! - Enter your email address.",
      "Type": "String",
      "MinLength": "6",
      "ConstraintDescription": "Email address cannot be empty."
    },
    "ec2startstop": {
      "Description": "The EC2 Scheduler automatically stops and restart EC2, key 'ec2-startstop'/'ec2-startstop:XXX' value '1000;1300;utc;mon,tue,fri', 'None' for no shutdown, default - 1130;2230;utc;weekdays",
      "Type": "String",
      "Default": "default"
    },
    "Project": {
      "Description": "Enter the name of the project",
      "Type": "String",
      "Default": "LeapFrog"
    },
    "Environment": {
      "Description": "REQUIRED! - Enter the Environment type: Dev/Test/QA/Prod/Perf/Other",
      "Type": "String",
      "AllowedValues": [
        "Development",
        "QA",
        "Production",
        "Performance",
        "Test",
        "Other"
      ],
      "Default": "QA"
    },
    "AppName": {
      "Description": "REQUIRED! - Enter the name of the application/service this instance will provide.",
      "Type": "String",
      "Default": "",
      "ConstraintDescription": "Go back and enter the application/service name for this instance"
    },
    "ServiceOwner": {
      "Description": "REQUIRED! - Enter the application owner for this server provides.",
      "Type": "String",
      "AllowedValues": [
        "Omni",
        "Database",
        "IPSS",
        "SAP",
        "Unix",
        "Other"
      ],
      "Default": "Omni"
    },
    "ServiceType": {
      "Description": "REQUIRED! - Enter the type of application.",
      "Type": "String",
      "AllowedValues": [
        "Application",
        "Database",
        "Web",
        "Other"
      ],
      "Default": "Application"
    },
    "OSType": {
      "Description": "REQUIRED! - Select the OS type",
      "Type": "String",
      "AllowedValues": [
        "RHEL",
        "SUSE",
        "AmazonLinux",
        "Windows",
        "Other"
      ],
      "Default": "RHEL"
    },
    "ClassificationFSI": {
      "Description": "Select FSI if it willcontain FSI data, otherwise leave blank.",
      "Type": "String",
      "Default": "",
      "AllowedValues": [
        "",
        "FSI"
      ]
    },
    "ClassificationPCI": {
      "Description": "Select PCI if it willcontain PCI data, otherwise leave blank.",
      "Type": "String",
      "Default": "",
      "AllowedValues": [
        "",
        "PCI"
      ]
    },
    "ClassificationPII": {
      "Description": "Select PII if it willcontain PII data, otherwise leave blank.?",
      "Type": "String",
      "Default": "",
      "AllowedValues": [
        "",
        "PII"
      ]
    },
    "BusinessImpact": {
      "Description": "REQUIRED! - What is the business impact of this service if there is an > 1 hour outage?",
      "Type": "String",
      "AllowedValues": [
        "High",
        "Med",
        "Low"
      ],
      "Default": "Low"
    },
    "InternalOrderNumber": {
      "Description": "REQUIRED! - Select the Internal Order Number as follows: OMNI:703976     SAP:703107     Membership:703129     General:703027     BI:703229    Security:703086",
      "Type": "String",
      "AllowedValues": [
        "703976",
        "703107",
        "703129",
        "703086",
        "703027",
        "703229"
      ],
      "ConstraintDescription": "Go back and select the appropriate Internal Order Number",
      "Default": "703976"
    },
    "InstanceType": {
      "Description": "EC2 instance type",
      "Type": "String",
      "Default": "t2.micro",
      "AllowedValues": [
        "t2.nano",
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "c3.large",
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "c4.large",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "x1.16xlarge",
        "x1.32xlarge"
      ],
      "ConstraintDescription": "Must be a valid EC2 instance type."
    },
    "ImageID": {
      "Description": "Select ImageID - [Privately license from RHEL 6.7=ami-90a8bbfa or 7.2=ami-95005f82 or 7.3=ami-a33668b4] or [Marketplace RHEL 6.7=ami-f37b4b99 or 7.2=ami-2051294a]",
      "Type": "String",
      "Default": "ami-a33668b4"
    },
    "ebssnapshot": {
      "Description": "default schedule 7 days 2100 US/Easter, custom: scheduler: key 'ebs-snapshot:1' Vault: '0800(time);15(retention time);us/eastern;all(all days of the week)' ex: 1000;2;us/eastern;weekend or https://s3.amazonaws.com/solutions-reference/ebs-snapshot-scheduler/latest/ebs-snapshot-scheduler.pdf ",
      "Type": "String",
      "Default": "default"
    },
    "ebssnapshot1": {
      "Description": "Default schedule 7 days 2100 US/Easter, custom: scheduler: key 'ebs-snapshot:1' Vault: '0800(time);7(retention time);us/eastern;all (all days of the week)' ex: 1000;2;us/eastern;weekend ",
      "Type": "String",
      "AllowedValues": [
        "",
        "0800;7;us/eastern;all",
        "1300;7;us/eastern;all",
        "1800;7;us/eastern;all"
      ]
    },
    "ebssnapshot2": {
      "Description": "Default schedule 7 days 2100 US/Easter, custom: scheduler: key 'ebs-snapshot:1' Vault: '0800(time);7(retention time);us/eastern;all (all days of the week)' ex: 1000;2;us/eastern;weekend ",
      "Type": "String",
      "AllowedValues": [
        "",
        "0800;7;us/eastern;all",
        "1300;7;us/eastern;all",
        "1800;7;us/eastern;all"
      ]
    },
    "ebssnapshot3": {
      "Description": "Default schedule 7 days 2100 US/Easter, custom: scheduler: key 'ebs-snapshot:1' Vault: '0800(time);7(retention time);us/eastern;all (all days of the week)' ex: 1000;2;us/eastern;weekend ",
      "Type": "String",
      "AllowedValues": [
        "",
        "0800;7;us/eastern;all",
        "1300;7;us/eastern;all",
        "1800;7;us/eastern;all"
      ]
    },
    "VolueSizeSDB": {
      "Description": "Change /sdb volume size if non-standard is required.",
      "Type": "String",
      "Default": "10"
    },
    "VolueSizeSDC": {
      "Description": "Change /sdc volume size if non-standard is required.",
      "Type": "String",
      "Default": "30"
    }
  },
  "Mappings": {},
  "Resources": {
    "InstanceRecovery": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "AlarmDescription": "Trigger a recovery when instance status check fails for 5 consecutive minutes.",
        "Namespace": "AWS/EC2",
        "MetricName": "StatusCheckFailed_System",
        "Statistic": "Minimum",
        "Period": "60",
        "EvaluationPeriods": "5",
        "ComparisonOperator": "GreaterThanThreshold",
        "Threshold": "0",
        "AlarmActions": [
          {
            "Fn::Join": [
              "",
              [
                "arn:aws:automate:",
                {
                  "Ref": "AWS::Region"
                },
                ":ec2:recover"
              ]
            ]
          }
        ],
        "Dimensions": [
          {
            "Name": "InstanceId",
            "Value": {
              "Ref": "EC2Instance"
            }
          }
        ]
      }
    },
    "EC2Instance": {
      "Type": "AWS::EC2::Instance",
      "DeletionPolicy": "Retain",
      "Properties": {
        "DisableApiTermination": {
          "Ref": "TerminationProtection"
        },
        "ImageId": {
          "Ref": "ImageID"
        },
        "SubnetId": {
          "Fn::Join": [
            ",",
            {
              "Ref": "SubNet"
            }
          ]
        },
        "InstanceType": {
          "Ref": "InstanceType"
        },
        "SecurityGroupIds": {
          "Ref": "SecurityGroupIds"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": {
              "Ref": "HostName"
            }
          },
          {
            "Key": "ServiceOwner",
            "Value": {
              "Ref": "ServiceOwner"
            }
          },
          {
            "Key": "AppName",
            "Value": {
              "Ref": "AppName"
            }
          },
          {
            "Key": "ServiceType",
            "Value": {
              "Ref": "ServiceType"
            }
          },
          {
            "Key": "Environment",
            "Value": {
              "Ref": "Environment"
            }
          },
          {
            "Key": "Project",
            "Value": {
              "Ref": "Project"
            }
          },
          {
            "Key": "CreatedBy",
            "Value": {
              "Ref": "CreatedBy"
            }
          },
          {
            "Key": "ClassificationFSI",
            "Value": {
              "Ref": "ClassificationFSI"
            }
          },
          {
            "Key": "ClassificationPCI",
            "Value": {
              "Ref": "ClassificationPCI"
            }
          },
          {
            "Key": "ClassificationPII",
            "Value": {
              "Ref": "ClassificationPII"
            }
          },
          {
            "Key": "BusinessImpact",
            "Value": {
              "Ref": "BusinessImpact"
            }
          },
          {
            "Key": "ebs-snapshot",
            "Value": {
              "Ref": "ebssnapshot"
            }
          },
          {
            "Key": "ebs-snapshot:1",
            "Value": {
              "Ref": "ebssnapshot1"
            }
          },
          {
            "Key": "ebs-snapshot:2",
            "Value": {
              "Ref": "ebssnapshot2"
            }
          },
          {
            "Key": "ebs-snapshot:3",
            "Value": {
              "Ref": "ebssnapshot3"
            }
          },
          {
            "Key": "OSType",
            "Value": {
              "Ref": "OSType"
            }
          },
          {
            "Key": "InternalOrderNumber",
            "Value": {
              "Ref": "InternalOrderNumber"
            }
          },
          {
            "Key": "ec2-startstop",
            "Value": {
              "Ref": "ec2startstop"
            }
          }
        ],
        "KeyName": {
          "Ref": "KeyName"
        },
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/sda1",
            "Ebs": {
              "VolumeType": "standard",
              "DeleteOnTermination": "true",
              "VolumeSize": "10"
            }
          },
          {
            "DeviceName": "/dev/sdb",
            "Ebs": {
              "VolumeType": "standard",
              "DeleteOnTermination": "true",
              "VolumeSize": {
                "Ref": "VolueSizeSDB"
              },
              "Encrypted": "true"
            }
          },
          {
            "DeviceName": "/dev/sdc",
            "Ebs": {
              "VolumeType": "gp2",
              "DeleteOnTermination": "true",
              "VolumeSize": {
                "Ref": "VolueSizeSDC"
              },
              "Encrypted": "true"
            }
          }
        ],
        "UserData": {
          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#!/bin/bash -x\n",
                "date > /root/bjsimage.log\n",
                "cat /etc/redhat-release >> /root/bjsimage.log\n",
                "mv /etc/resolv.conf /etc/resolv.conf.original\n",
                "touch /etc/resolv.conf\n",
                "echo \"search bjw2k.asg\" >> /etc/resolv.conf\n",
                "echo \"nameserver 10.250.1.21\" >> /etc/resolv.conf\n",
                "echo \"nameserver 10.10.65.20\" >> /etc/resolv.conf\n",
                "cat /etc/resolv.conf >> /root/bjsimage.log\n",
                "echo ",
                {
                  "Ref": "HostName"
                },
                " > /etc/hostname\n",
                "hostname ",
                {
                  "Ref": "HostName"
                },
                ".bjw2k.asg\n",
                "hostnamectl set-hostname ",
                {
                  "Ref": "HostName"
                },
                ".bjw2k.asg\n",
                "mv /etc/hosts /etc/hosts.original\n",
                "touch /etc/hosts\n",
                "echo \"127.0.0.1  ",
                {
                  "Ref": "HostName"
                },
                "  localhost.localdomain localhost4 localhost4.localdomain4\" >> /etc/hosts\n",
                "echo \"::1         localhost localhost.localdomain localhost6 localhost6.localdomain6\" >> /etc/hosts\n",
                "cat /etc/hosts >> /root/bjsimage.log\n",
                "echo \"preserve_hostname: true\" >> /etc/cloud/cloud.cfg\n",
                "mv /etc/sysconfig/network /etc/sysconfig/network.original\n",
                "touch /etc/sysconfig/network\n",
                "echo \"NETWORKING=yes\" >> /etc/sysconfig/network\n",
                "echo \"NETWORKING_IPV6=no\" >> /etc/sysconfig/network\n",
                "echo \"NOZEROCONF=yes\" >> /etc/sysconfig/network\n",
                "echo \"HOSTNAME=",
                {
                  "Ref": "HostName"
                },
                ".bjw2k.asg\" >> /etc/sysconfig/network\n",
                "cat /etc/sysconfig/network >> /root/bjsimage.log\n",
                "rm -f /etc/yum.repos.d/*\n",
                "# curl -k https://ua00puppmp01.bjw2k.asg:8140/packages/current/install.bash | bash\n",
                "# chkconfig puppet on\n",
                "# puppet  agent -t --server=ua00puppmp01.bjw2k.asg\n",
                "# service puppet status\n",
                "date >> /root/bjsimage.log\n"
              ]
            ]
          }
        }
      }
    }
  },
  "Outputs": {
    "InstanceId": {
      "Description": "InstanceId of the newly created EC2 instance",
      "Value": {
        "Ref": "EC2Instance"
      }
    },
    "InstanceType": {
      "Description": "Instance type of the newly created EC2 instance",
      "Value": {
        "Ref": "InstanceType"
      }
    },
    "HostName": {
      "Description": "Hostname of the newly created EC2 instance",
      "Value": {
        "Ref": "HostName"
      }
    },
    "AZ": {
      "Description": "Availability Zone of the newly created EC2 instance",
      "Value": {
        "Fn::GetAtt": [
          "EC2Instance",
          "AvailabilityZone"
        ]
      }
    }
  }
}
